package com.orxeira.pruebatecnica.presentation.buildinglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.orxeira.pruebatecnica.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_building.view.*
import java.util.*

class BuildingListAdapter constructor(val callback: OnBuildingSelectedCallback) :
    RecyclerView.Adapter<BuildingListAdapter.BuildingListViewHolder>() {

    private val buildingsList = ArrayList<BuildingWrapper>()

    private var filterFloors = false
    private var filterPOIs = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuildingListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_building, parent, false)
        return BuildingListViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: BuildingListViewHolder, position: Int) {
        holder.bind(getFilteredList()[position])
        holder.itemView.setOnClickListener {
            callback.onBuildingSelected(getFilteredList()[position])
        }
    }

    override fun getItemCount(): Int {
        return getFilteredList().size
    }

    internal fun setBuildingsList(buildings: List<BuildingWrapper>) {
        this.buildingsList.clear()
        this.buildingsList.addAll(buildings)
        notifyDataSetChanged()
    }

    fun filterFloors(filtered: Boolean) {
        filterFloors = filtered
        notifyDataSetChanged()
    }

    fun filterPois(filtered: Boolean) {
        filterPOIs = filtered
        notifyDataSetChanged()
    }

    private fun getFilteredList(): List<BuildingWrapper> {
        var auxList: List<BuildingWrapper> = buildingsList
        //filter list by floors
        if (filterFloors) auxList =
            auxList.filter { item -> item.floors.isNullOrEmpty().not() && item.floors!!.count() > 1 }
        //filter list by POIs
        if (filterPOIs) auxList = auxList.filter { item -> item.pois.isNullOrEmpty().not() }

        return auxList
    }

    class BuildingListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(buildingWrapper: BuildingWrapper) {

            itemView.buildingName.text = buildingWrapper.building.name
            if (buildingWrapper.floors.isNullOrEmpty()) {
                itemView.floorsCount.visibility = View.GONE
            } else {
                itemView.floorsCount.visibility = View.VISIBLE
                itemView.floorsCount.text = itemView.context.getString(
                    R.string.floorCount,
                    buildingWrapper.floors?.size?.toString()
                )
            }

            if (buildingWrapper.pois.isNullOrEmpty()) {
                itemView.poisCount.visibility = View.GONE
            } else {
                itemView.poisCount.visibility = View.VISIBLE
                itemView.poisCount.text = itemView.context.getString(
                    R.string.poiCount,
                    buildingWrapper.pois?.size?.toString()
                )
            }
        }
    }

    interface OnBuildingSelectedCallback {
        fun onBuildingSelected(buildingWrapper: BuildingWrapper)
    }
}
