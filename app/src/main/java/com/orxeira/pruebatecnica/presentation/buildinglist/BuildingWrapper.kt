package com.orxeira.pruebatecnica.presentation.buildinglist

import es.situm.sdk.model.cartography.Building
import es.situm.sdk.model.cartography.Floor
import es.situm.sdk.model.cartography.Poi

data class BuildingWrapper(var building: Building, var floors: List<Floor>?, var pois: List<Poi>?)