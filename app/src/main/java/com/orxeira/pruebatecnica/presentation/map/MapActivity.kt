package com.orxeira.pruebatecnica.presentation.map

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.orxeira.pruebatecnica.R
import com.orxeira.pruebatecnica.data.Cache
import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.usecases.GetDirections
import com.orxeira.pruebatecnica.data.usecases.GetFloorMapUseCase
import com.orxeira.pruebatecnica.data.usecases.GetPoiIconUserCase
import com.orxeira.pruebatecnica.presentation.buildinglist.BuildingWrapper
import es.situm.sdk.directions.DirectionsRequest
import es.situm.sdk.error.Error
import es.situm.sdk.location.util.CoordinateConverter
import es.situm.sdk.model.cartography.Poi
import es.situm.sdk.model.cartography.Point
import es.situm.sdk.model.directions.Route
import es.situm.sdk.model.location.Angle
import es.situm.sdk.model.location.Coordinate
import kotlinx.android.synthetic.main.activity_map.*
import java.util.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object {
        const val ARG_BULIDING_WRAPPER = "MapActivity.ARG_BULIDING_WRAPPER"
    }

    private var map: GoogleMap? = null
    private var buildingWrapper: BuildingWrapper? = null
    private val polylines = ArrayList<Polyline>()
    private var markerDestination: Marker? = null
    private var markerOrigin: Marker? = null
    private var pointOrigin: Point? = null
    private var coordinateConverter: CoordinateConverter? = null
    private var floorId: String? = null

    private val buildingExtra: BuildingWrapper?
        get() {
            if (intent.hasExtra(ARG_BULIDING_WRAPPER)) {
                val id = intent.extras?.get(ARG_BULIDING_WRAPPER) as String?
                Cache.buildings?.let { cachedBuildings ->
                    for (i in cachedBuildings) {
                        if (i.building.identifier == id) {
                            return i
                        }
                    }
                }
            }
            return null
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        buildingWrapper = buildingExtra

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = buildingWrapper?.building?.name

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        buildingWrapper?.building?.let { building ->
            coordinateConverter = CoordinateConverter(
                building.dimensions,
                building.center,
                building.rotation
            )
        }
        fetchFirstFloorImage(object : Callback {
            override fun onSuccess(floorImage: Bitmap) {
                drawBuilding(floorImage)
            }

            override fun onError(error: Error) {
                Snackbar.make(
                    findViewById<View>(R.id.container),
                    error.message,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .show()
            }
        })

        getPois()

        map?.setOnMarkerClickListener(this)

    }

    internal fun drawBuilding(bitmap: Bitmap) {
        buildingWrapper?.let {
            val drawBounds = it.building.bounds
            val coordinateNE = drawBounds.northEast
            val coordinateSW = drawBounds.southWest
            val latLngBounds = LatLngBounds(
                LatLng(coordinateSW.latitude, coordinateSW.longitude),
                LatLng(coordinateNE.latitude, coordinateNE.longitude)
            )

            map?.addGroundOverlay(
                GroundOverlayOptions()
                    .image(BitmapDescriptorFactory.fromBitmap(bitmap))
                    .bearing(it.building.rotation.degrees().toFloat())
                    .positionFromBounds(latLngBounds)
            )

            map?.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100))
        }

    }

    private fun getPois() {
        buildingWrapper?.pois?.let { pois ->
            if (pois.isEmpty()) {
                Toast.makeText(
                    this,
                    "There isnt any poi in the building: " + buildingWrapper?.building?.name + ". Go to the situm dashboard and create at least one poi before execute again this example",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                for (poi in pois) {
                    GetPoiIconUserCase().invoke(GetPoiIconUserCase.Params(poi)) { result ->
                        when (result) {
                            is Result.Response -> drawPoi(poi, result.data)
                            is Result.Error -> drawPoi(poi)
                        }
                    }
                }
            }
        } ?: Toast.makeText(
            this,
            "There isnt any poi in the building: " + buildingWrapper?.building?.name + ". Go to the situm dashboard and create at least one poi before execute again this example",
            Toast.LENGTH_LONG
        ).show()


    }

    private fun drawPoi(poi: Poi, bitmap: Bitmap? = null) {
        val latLng = LatLng(
            poi.coordinate.latitude,
            poi.coordinate.longitude
        )
        val markerOptions = MarkerOptions()
            .position(latLng)
            .title(poi.name)
        if (bitmap != null) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
        }
        map?.addMarker(markerOptions)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        p0?.let { poi ->

            if (pointOrigin == null) {
                clearMap()
                pointOrigin = createPoint(poi.position)
            } else {
                calculateRoute(poi.position)
            }

        }

        return false
    }

    private fun calculateRoute(latLng: LatLng) {
        pointOrigin?.let { origin ->
            val pointDestination = createPoint(latLng)
            val directionsRequest = DirectionsRequest.Builder()
                .from(origin, Angle.EMPTY)
                .to(pointDestination)
                .build()

            GetDirections().invoke(GetDirections.Params(directionsRequest)) { result ->
                when (result) {
                    is Result.Response -> {
                        with(result.data) {
                            drawRoute(this)
                            centerCamera(this)
                            pointOrigin = null
                        }

                    }
                    is Result.Error -> {
                        Toast.makeText(this@MapActivity, result.error.message, Toast.LENGTH_LONG)
                            .show()
                    }
                }
            }
        }
    }

    private fun drawRoute(route: Route) {
        val floors = mutableListOf<String>()
        for (segment in route.segments) {
            if (floors.contains(segment.floorIdentifier).not()) {
                floors.add(segment.floorIdentifier)
            }
            //For each segment you must draw a polyline
            //Add an if to filter and draw only the current selected floor
            val latLngs = ArrayList<LatLng>()
            for (point in segment.points) {
                latLngs.add(LatLng(point.coordinate.latitude, point.coordinate.longitude))
            }

            val polyLineOptions = PolylineOptions()
                .color(Color.GREEN)
                .width(4f)
                .addAll(latLngs)
            polylines.add(map!!.addPolyline(polyLineOptions))
        }

        val floorChanges = route.segments.size - 1
        val distance = route.distance

        tvFloorChanges.text = getString(R.string.floor_changes, floorChanges)
        tvDistance.text = getString(R.string.distance, distance)

        clExtendedInfo.visibility = View.VISIBLE
    }

    private fun createPoint(latLng: LatLng): Point {
        val coordinate = Coordinate(latLng.latitude, latLng.longitude)
        val cartesianCoordinate = coordinateConverter!!.toCartesianCoordinate(coordinate)
        return Point(
            buildingWrapper!!.building.identifier,
            floorId!!,
            coordinate,
            cartesianCoordinate
        )
    }

    private fun centerCamera(route: Route) {
        val from = route.from.coordinate
        val to = route.to.coordinate

        val builder = LatLngBounds.Builder()
            .include(LatLng(from.latitude, from.longitude))
            .include(LatLng(to.latitude, to.longitude))
        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100))
    }

    private fun clearMap() {
        removePolylines()
        clExtendedInfo.visibility = View.GONE
    }

    private fun removePolylines() {
        for (polyline in polylines) {
            polyline.remove()
        }
        polylines.clear()
    }

    private fun fetchFirstFloorImage(callback: Callback) {
        buildingWrapper?.floors?.let {
            floorId = it[0].identifier
            GetFloorMapUseCase().invoke(GetFloorMapUseCase.Params(it[0])) { result ->
                when (result) {
                    is Result.Response -> {
                        callback.onSuccess(result.data)
                    }
                    is Result.Error -> {
                        callback.onError(result.error)
                    }
                }
            }
        }
    }

    internal interface Callback {
        fun onSuccess(floorImage: Bitmap)

        fun onError(error: Error)
    }


}
