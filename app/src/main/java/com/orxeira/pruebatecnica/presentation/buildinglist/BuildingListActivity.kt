package com.orxeira.pruebatecnica.presentation.buildinglist

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.orxeira.pruebatecnica.R
import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.usecases.GetBuildingsDataUseCase
import com.orxeira.pruebatecnica.presentation.map.MapActivity
import kotlinx.android.synthetic.main.activity_building_list.*

class BuildingListActivity : AppCompatActivity(),
    BuildingListAdapter.OnBuildingSelectedCallback {

    private var adapter: BuildingListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_building_list)


        val linearLayoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = linearLayoutManager

        adapter = BuildingListAdapter(this)
        recyclerView!!.adapter = adapter

        initFilters()
        getBuildings()
    }


    private fun getBuildings() {
        progressBar!!.visibility = RecyclerView.VISIBLE
        listContainer!!.visibility = View.GONE

        GetBuildingsDataUseCase()
            .invoke(GetBuildingsDataUseCase.Params()) { result ->
                when (result) {
                    is Result.Error -> {
                        progressBar.visibility = View.GONE
                        result.error.let {
                            Snackbar.make(container, it.message, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.retry) { getBuildings() }
                                .show()
                        }
                    }
                    is Result.Response -> {
                        progressBar.visibility = View.GONE
                        listContainer.visibility = View.VISIBLE
                        adapter?.setBuildingsList(result.data)
                    }
                }
            }
    }

    private fun initFilters() {
        filterFloors.setOnCheckedChangeListener { _, isChecked ->
            adapter?.filterFloors(isChecked)
        }

        filterPOIs.setOnCheckedChangeListener { _, isChecked ->
            adapter?.filterPois(isChecked)
        }
    }

    override fun onBuildingSelected(buildingWrapper: BuildingWrapper) {
        val intent = Intent(this, MapActivity::class.java)
        intent.putExtra(MapActivity.ARG_BULIDING_WRAPPER, buildingWrapper.building.identifier)
        startActivity(intent)
    }
}
