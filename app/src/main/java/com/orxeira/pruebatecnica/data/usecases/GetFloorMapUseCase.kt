package com.orxeira.pruebatecnica.data.usecases

import android.graphics.Bitmap
import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.base.UseCase
import com.orxeira.pruebatecnica.data.services.GetFloorMapService
import es.situm.sdk.model.cartography.Floor
import kotlinx.coroutines.runBlocking

class GetFloorMapUseCase : UseCase<Bitmap, GetFloorMapUseCase.Params>() {

    override suspend fun run(params: Params): Result<Bitmap> {
        return runBlocking {
            GetFloorMapService(params.floor).runService()
        }
    }

    class Params(internal val floor: Floor)
}