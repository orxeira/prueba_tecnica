package com.orxeira.pruebatecnica.data.services

import com.orxeira.pruebatecnica.data.base.Result
import es.situm.sdk.SitumSdk
import es.situm.sdk.directions.DirectionsRequest
import es.situm.sdk.error.Error
import es.situm.sdk.model.directions.Route
import es.situm.sdk.utils.Handler
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetDirectionsService(private val request: DirectionsRequest) {
    private var continuation: Continuation<Result<Route>>? = null

    suspend fun runService(): Result<Route> {
        return suspendCoroutine { continuation ->
            this.continuation = continuation

            SitumSdk.directionsManager().requestDirections(request, object :
                Handler<Route> {

                override fun onSuccess(p0: Route) {
                    continuation.resume(Result.Response(p0))
                }

                override fun onFailure(p0: Error) {
                    continuation.resume(Result.Error(p0))
                }
            })
        }
    }

}