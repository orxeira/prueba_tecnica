package com.orxeira.pruebatecnica.data.services

import com.orxeira.pruebatecnica.data.base.Result
import es.situm.sdk.SitumSdk
import es.situm.sdk.model.cartography.Building
import es.situm.sdk.model.cartography.Poi
import es.situm.sdk.utils.Handler
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetPOIService(private val building: Building) {
    private var continuation: Continuation<Result<List<Poi>>>? = null

    suspend fun runService(): Result<List<Poi>> {
        return suspendCoroutine { continuation ->
            this.continuation = continuation

            SitumSdk.communicationManager().fetchIndoorPOIsFromBuilding(building, object :
                Handler<Collection<Poi>> {

                override fun onSuccess(p0: Collection<Poi>) {
                    continuation.resume(Result.Response(ArrayList(p0)))
                }

                override fun onFailure(p0: es.situm.sdk.error.Error) {
                    continuation.resume(Result.Error(p0))
                }
            })
        }
    }
}