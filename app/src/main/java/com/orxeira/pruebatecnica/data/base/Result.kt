package com.orxeira.pruebatecnica.data.base

sealed class Result<out T : Any> {

    class Response<out T : Any>(val data: T) : Result<T>()

    class Error(val error: es.situm.sdk.error.Error) : Result<Nothing>()
}