package com.orxeira.pruebatecnica.data

import com.orxeira.pruebatecnica.presentation.buildinglist.BuildingWrapper

object Cache {
    var buildings: MutableList<BuildingWrapper>? = null
}