package com.orxeira.pruebatecnica.data.services

import com.orxeira.pruebatecnica.data.base.Result
import es.situm.sdk.SitumSdk
import es.situm.sdk.error.Error
import es.situm.sdk.model.cartography.Building
import es.situm.sdk.model.cartography.Floor
import es.situm.sdk.utils.Handler
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetFloorsService(private val building: Building) {
    private var continuation: Continuation<Result<List<Floor>>>? = null

    suspend fun runService(): Result<List<Floor>> {
        return suspendCoroutine { continuation ->
            this.continuation = continuation

            SitumSdk.communicationManager().fetchFloorsFromBuilding(building, object :
                Handler<Collection<Floor>> {

                override fun onSuccess(p0: Collection<Floor>) {
                    continuation.resume(Result.Response(ArrayList(p0)))
                }

                override fun onFailure(p0: Error) {
                    continuation.resume(Result.Error(p0))
                }
            })
        }
    }
}