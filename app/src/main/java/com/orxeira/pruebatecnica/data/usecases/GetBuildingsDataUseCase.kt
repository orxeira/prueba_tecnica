package com.orxeira.pruebatecnica.data.usecases

import com.orxeira.pruebatecnica.data.Cache
import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.base.UseCase
import com.orxeira.pruebatecnica.data.services.GetBuildingsService
import com.orxeira.pruebatecnica.data.services.GetFloorsService
import com.orxeira.pruebatecnica.data.services.GetPOIService
import com.orxeira.pruebatecnica.presentation.buildinglist.BuildingWrapper
import kotlinx.coroutines.runBlocking

class GetBuildingsDataUseCase : UseCase<List<BuildingWrapper>, GetBuildingsDataUseCase.Params>() {

    override suspend fun run(params: Params): Result<List<BuildingWrapper>> {

        if (Cache.buildings.isNullOrEmpty().not()) return Result.Response(
            Cache.buildings!!.toList()
        )

        val response = runBlocking {
            GetBuildingsService().runService()
        }

        when (response) {
            is Result.Response -> {

                val buildingsList = mutableListOf<BuildingWrapper>()

                response.data.forEach { building ->
                    val buildingWrapper =
                        BuildingWrapper(
                            building,
                            null,
                            null
                        )
                    runBlocking {
                        GetPOIService(building).runService()
                    }.also { poiResponse ->
                        if (poiResponse is Result.Response) buildingWrapper.pois = poiResponse.data
                    }

                    runBlocking {
                        GetFloorsService(building).runService()
                    }.also { floorResponse ->
                        if (floorResponse is Result.Response) buildingWrapper.floors =
                            floorResponse.data
                    }
                    buildingsList.add(buildingWrapper)
                }
                Cache.buildings = buildingsList
                return Result.Response(buildingsList)
            }

            is Result.Error -> return response
        }
    }

    class Params
}