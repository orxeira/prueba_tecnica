package com.orxeira.pruebatecnica.data.services

import android.graphics.Bitmap
import com.orxeira.pruebatecnica.data.base.Result
import es.situm.sdk.SitumSdk
import es.situm.sdk.error.Error
import es.situm.sdk.model.cartography.Floor
import es.situm.sdk.utils.Handler
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetFloorMapService(val floor: Floor) {
    private var continuation: Continuation<Result<Bitmap>>? = null

    suspend fun runService(): Result<Bitmap> {
        return suspendCoroutine { continuation ->
            this.continuation = continuation

            SitumSdk.communicationManager().fetchMapFromFloor(floor, object :
                Handler<Bitmap> {

                override fun onSuccess(p0: Bitmap) {
                    continuation.resume(Result.Response(p0))
                }

                override fun onFailure(p0: Error) {
                    continuation.resume(Result.Error(p0))
                }
            })
        }
    }
}