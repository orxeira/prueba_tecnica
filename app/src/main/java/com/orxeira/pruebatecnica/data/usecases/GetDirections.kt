package com.orxeira.pruebatecnica.data.usecases

import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.base.UseCase
import com.orxeira.pruebatecnica.data.services.GetDirectionsService
import es.situm.sdk.directions.DirectionsRequest
import es.situm.sdk.model.directions.Route
import kotlinx.coroutines.runBlocking

class GetDirections : UseCase<Route, GetDirections.Params>() {

    override suspend fun run(params: GetDirections.Params): Result<Route> {
        return runBlocking {
            GetDirectionsService(params.directionsRequest).runService()
        }
    }

    class Params(internal val directionsRequest: DirectionsRequest)
}