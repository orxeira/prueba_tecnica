package com.orxeira.pruebatecnica.data.usecases

import android.graphics.Bitmap
import com.orxeira.pruebatecnica.data.base.Result
import com.orxeira.pruebatecnica.data.base.UseCase
import com.orxeira.pruebatecnica.data.services.GetPoiIconService
import es.situm.sdk.model.cartography.Poi
import kotlinx.coroutines.runBlocking

class GetPoiIconUserCase : UseCase<Bitmap, GetPoiIconUserCase.Params>() {

    override suspend fun run(params: Params): Result<Bitmap> {
        return runBlocking {
            GetPoiIconService(params.poi).runService()
        }
    }

    class Params(internal val poi: Poi)
}