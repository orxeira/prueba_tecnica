package com.orxeira.pruebatecnica.application;

import androidx.multidex.MultiDexApplication;

import es.situm.sdk.SitumSdk;

public class PruebaTecnica extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        SitumSdk.init(this);
    }
}
